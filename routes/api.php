<?php

use App\Http\Controllers\TesteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::controller(TesteController::class)->group(function() {
    #    Route::get('/teste', 'novoAtalho');
    #    Route::get('/teste/{texto}', 'novoAtalho');
    Route::group(['prefix' => 'teste'], function() {
        Route::get('/', 'novoAtalho');
        Route::get('/{texto}', 'escrevComParam');
    });
        
        Route::group(['prefix' => 'cliente'], function() {
            Route::get('/', 'novoAtalho');
            Route::get('/{texto}', 'escrevComParam');
        });
});