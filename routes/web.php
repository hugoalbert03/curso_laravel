<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\AutenticarController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 
/*Route::get('/', function () {
    return view('cliente/cadastro');
});*/


Route::get('/cadastro', [ClienteController::class, 'view'])->name('cadastro');
Route::post('/cadastrar', [ClienteController::class, 'store'])->name('cadastrar');
Route::post('/alterar', [ClienteController::class, 'alterar'])->name('alterar');
#Route::get('/', [ClienteController::class, 'listarClientes'])->name('listarClientes');
Route::get('/telaEditarCliente/{id}', [ClienteController::class, 'telaEditarCliente'])->name('telaEditarCliente');
Route::get('/visualizarCliente/{idVis}', [ClienteController::class, 'visualizarCliente'])->name('visualizarCliente');
Route::get('/excluirCliente/{idCli}', [ClienteController::class, 'excluirCliente'])->name('excluirCliente');
Route::get('/excluirClienteLogico/{idCliLogic}', [ClienteController::class, 'excluirClienteLogico'])->name('excluirClienteLogico');
#Route::get('/Cadastro',[ClienteController::class,'escreverString']);
#Route::group(['middleware' => ['auth','api']], function() {
    /* rotas protegidas...*/
/*
    Route::controller(TesteController::class)->group(function() {
        #    Route::get('/teste', 'novoAtalho');
        #    Route::get('/teste/{texto}', 'novoAtalho');
        Route::group(['prefix' => 'teste'], function() {
            Route::get('/', 'novoAtalho');
            Route::get('/{texto}', 'escrevComParam');
        });
            
        Route::group(['prefix' => 'cliente'], function() {
            Route::get('/', 'novoAtalho');
            Route::get('/{texto}', 'escrevComParam');
        });
    });
*/
#});

/* Route::group(['prefix' => 'teste'], function() {
    Route::get('/', 'novoAtalho');
    Route::get('/{nome}', 'novoAtalho');
}); */

Route::view('/', 'login')->name('login');
Route::post('/logar', [AutenticarController::class, 'logar'])->name('logar');