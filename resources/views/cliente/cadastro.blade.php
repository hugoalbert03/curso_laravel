@extends('layout/template')
@section('content')
<div class="container">
	<h1>{{isset($cliente->nm_cliente) ? "Alterar Cliente":"Novo Cliente"}}</h1>	
	<form action="{{isset($cliente->id_cliente)?'/alterar':'/cadastrar'}}" class="" method="post">
		@if($errors->any())
			<div class="alert alert-danger auto-fechar">
				@foreach($errors->all() as $error)
					<p>  {{ $error }} </p>
				@endforeach
			</div>
		@endif
		@csrf
		<input type="hidden" name="id_cliente" value="{{isset($cliente->nm_cliente)? $cliente->id_cliente : ''}}">
		<p>
			<label>Nome do cliente</label><br>
			<input type ="text" name="nm_cliente" value="{{isset($cliente->nm_cliente)? $cliente->nm_cliente : old('nm_cliente','')}}" id="nm_cliente" class="form-control">
		</p>
		<p>
			<label>Endereço do cliente</label><br>
			<input type ="text" name="ds_endereco" value="{{isset($cliente->ds_endereco)? $cliente->ds_endereco : old('ds_endereco')}}" id="ds_endereco" class="form-control">
		</p>
		<button type="submit" class="btn btn-primary">Enviar</button>
		<p>
			@if(session('mensagem'))
			<div class ="alert alert-success auto-fechar text-center">
				<strong>{{session('mensagem')}}</strong>
			</div>
			@endif
		</p>
	</form>
</div>
@endsection