@extends('layout/template')
@section('content')
<div class="container">
	<h1>Cliente</h1>
		@csrf	
		<p>
			<label>Nome do cliente</label>:&nbsp;<strong>{{$cliente->nm_cliente}}</strong>
			
		</p>
		<p>
			<label>Endereço do cliente</label>:&nbsp;<strong><?php echo $cliente->ds_endereco ?></strong>
			<!--<input type ="text" name="ds_endereco" value="{{isset($cliente->ds_endereco)? old($cliente->ds_endereco) : '' }}" id="ds_endereco" class="form-control">-->
			
		</p>
		<button type="submit" onclick="history.back()" class="btn btn-primary">Voltar</button>		
</div>
@endsection