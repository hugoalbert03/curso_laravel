@extends('layout/template')
@section('content')
<div class="container">
	<h1>Listar Cliente</h1>	
	<p>
		@if(session('msg'))
			<div class="alert alert-warning auto-fechar text-center">
				<strong>{{session('msg')}}</strong>
			</div>
		@endif
	</p>
	<button class="btn btn-success" onclick="document.location='/cadastro'">Cadastrar</button>
	<table class="table">
		<tr>
			<th>Ref</th>
			<th>Nome</th>
			<th>Endereço</th>
			<th>Data de Inclusão</th>
			<th>Ações</th>
		</tr>
		@foreach($dados as $lista)
			<tr>
				<td>{{$lista->id_cliente}}</td> <!--ID Cliente-->
				<td>{{$lista->nm_cliente}}</td> <!--Nome do Cliente-->
				<td>{{$lista->ds_endereco}}</td> <!--Endereço-->
				<td>{{$lista->dt_inc}}</td> <!--Data de Inclusão-->
				<td>
					<a href="visualizarCliente/{{$lista->id_cliente}}">Visualizar</a>&nbsp;
					<a href="telaEditarCliente/{{$lista->id_cliente}}">Editar</a>&nbsp;
					<a onclick="excluir({{$lista->id_cliente}})" href="#">Excluir</a>
					<a onclick="excluirLogico({{$lista->id_cliente}})" href="#">Excluir Lógico</a>
				</td>
			</tr>
		@endforeach
	</table>
	<script>
		function excluir(id){
			if(confirm('Deseja realmente excluir o registro?')){
				document.location='/excluirCliente/'+id;
			}else{
				return false;
			}
		}

		function excluirLogico(id){
			if(confirm('Deseja realmente excluir o registro?')){
				document.location='/excluirClienteLogico/'+id;
			}else{
				return false;
			}
		}
	</script>
</div>
@endsection