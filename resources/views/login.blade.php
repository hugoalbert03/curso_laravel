<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <style>
            body{
                font-family:'Nunito', sans-serif;
            }
        </style>
        <title>{{ env("APP_NAME") }}</title>
    </head>
    <body class="container">
    	<form  method="POST" action="/logar">
            @csrf
        	<p>
            	<label for="email">Login:</label>
                <input type="email" placeholder="exemplo@email.com" id="email" class="form-control" name="acesso">
        	</p>
        	<p>
                <label for="senha">Senha:</label>
                <input type="password" placeholder="Digite sua senha" id="senha" class="form-control" name="senha">
        	<br>
            <button type="submit" class="btn btn-primary">Autenticar</button>
            </p>
    	</form>
        
        
    </body>
</html>
