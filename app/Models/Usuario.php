<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
# use Illuminate\Database\Eloquent\Model;
use illuminate\Foundation\Auth\User as Authenticatable;
use illuminate\Notifications\Notifiable;

class Usuario extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = "tb_usuario";
    protected $primaryKey = "id_usuario";

    public $timestamps = false;

    protected $filables = [
        'nm_usuario',
        'ds_login',
        'ds_senha',
        'fl_ativo'
    ];

    protected $hidden = [
        'ds_senha'
    ];
}
