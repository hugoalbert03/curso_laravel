<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Cliente;
class ClienteController extends Controller
{
    private $rules = [
        
        "nm_cliente" => "required|max:80|email",
    ];
    private $messages = [
        'required' =>'Campos obrigatórios não informados.',
        'max' => 'Limte de caracteres',
        'email' => "Email inválido"
    ];
    public function view(){
        return view('cliente.cadastro');
    }
    
    public function store(Request $request){
        $validar = Validator::make($request->all(),$this->rules,$this->messages);
        if($validar->fails()){
            return back()->withErrors($validar->errors())->withInput();
        } else {
            Cliente::create($request->all());
        
            #return redirect(Route('cliente/cadastro'));
            return redirect(route('cadastro'))->with("mensagem", "Cadastrado com sucesso!");
        }
        
    }

    public function listarClientes(){
        $lista = Cliente::all();
        return view('cliente.listar', ['dados' => $lista]);
    }

    public function telaEditarCliente($idCliente){
        $cliente = Cliente::find($idCliente);
        #$cliente = Cliente::where('fl_ativo', 1)->find($idCliente);
        #dd($cliente);
        return view('cliente.cadastro',['cliente' => $cliente]);
    }

    public function alterar(Request $request){
        #$Cliente = Cliente::where('id_cliente',$request->id_cliente)->first(); 
        $cliente = $this->recuperarUmCliente($request->id_cliente);
        $cliente->update($request->all());
        return $this->listarClientes();
        dd();
    }


    public function visualizarCliente($idVisualizar){
        $cliente = Cliente::where('id_cliente', $idVisualizar)->first();
        $cliente = $this->recuperarUmCliente($idVisualizar);
        return view('cliente.visualizar',['cliente' => $cliente]);
    }

    private function recuperarUmCliente($id){
        return Cliente::where('id_cliente',$id)->first();
    }

    public function excluirCliente($idCli){
        $cliente = $this->recuperarUmCliente($idCli);

        if($cliente){
            $cliente->delete();
            #return $this->listarClientes();
            return redirect(route('listarClientes'))->with("msg","Excluido com sucesso!");
        } else {
            return redirect(route('listarClientes'))->with("msg","Registro inválido!");
        }
    }

    public function excluirClienteLogico($idCliLogic){
        $cliente = $this->recuperarUmCliente($idCliLogic);

        if($cliente){
            $cliente->update(['fl_ativo' => 0]);
            #return $this->listarClientes();
            return redirect(route('listarClientes'))->with("msg","Excluido com sucesso!");
        } else {
            return redirect(route('listarClientes'))->with("msg","Registro inválido!");
        }
    }
}
