<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TesteController extends Controller
{
    public function novoAtalho(Request $request){
        #dd($request -> texto);
        #return "Seja Bem Vindo(a) ". $request -> texto;
        return view('test/teste');
    }

    public function escrevComParam(Request $request){
        #dd($request -> texto);
        #return "Nome do cliente: ". $request -> texto;
        return view('test/param');
    }

    private function funcaoPrivada(){
        return "Classe Privada";
    }

    public function mostrandoFuncaoPrivada(){
        return $this -> funcaoPrivada();
    }
    
    
}
